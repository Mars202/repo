var Bicicleta = require('../../modules/Bicicleta');

exports.bicicleta_list = function(req, res){
    res.status(200).json({
        Bicicletas: Bicicleta.allBicis
    })
}

exports.bicicleta_create = function(req, res){
    var bici = new Bicicleta(req.body.id,req.body.modelo, req.body.color);

    bici.ubi = [req.body.lat, req.body.lon];

    Bicicleta.add(bici);

    res.status(200).json({
        bicicleta: bici
    })
}

exports.bicicleta_delete = function(req,res){
    Bicicleta.removeById(req.body.id);

    res.status(204).send();
}

var Bicicleta = function(id, modelo, color, ubi){
    this.id = id,
    this.modelo = modelo,
    this.color = color,
    this.ubi = ubi
}

Bicicleta.prototype.toString = function() {
    return("id: "+ this.id + " color: " + this.color)
}

Bicicleta.allBicis = []; 

Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);

}

Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    
    if(aBici)
        return aBici;
    else
        throw new Error('No existe una bicicleta con el id: ' + aBiciId);
}

Bicicleta.removeById = function(aBiciId){
    Bicicleta.findById(aBiciId);
    for (var i = 0; i < Bicicleta.allBicis.length; i++) {
        
        if(Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i,1);
            break;
        }
    }
}



var a = new Bicicleta(1 ,"Montanera", "rojo", [11.191535, -68.021152]);
var b = new Bicicleta(2 ,"Carreras", "azul", [12.191535, -68.021152]);


Bicicleta.add(a);
Bicicleta.add(b);


module.exports = Bicicleta;
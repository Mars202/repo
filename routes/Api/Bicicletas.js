var express = require('express');
var router = express.Router();
var bicicletaController = require("../../Controllers/Api/BicicletaControllerApi");

router.get('/', bicicletaController.bicicleta_list);
router.post('/Create', bicicletaController.bicicleta_create);
router.delete('/Delete', bicicletaController.bicicleta_delete);


module.exports = router;
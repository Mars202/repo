var express = require('express');
var router = express.Router();
var bicicletaController = require("../Controllers/Bicicleta");

router.get('/', bicicletaController.bicicleta_list);
router.get('/Create', bicicletaController.bicicleta_create_get);
router.post('/Create', bicicletaController.bicicleta_create_post);

router.get('/:id/Update', bicicletaController.bicicleta_update_get);
router.post(`/:id/Update`, bicicletaController.bicicleta_update_post);


router.post('/:id/Delete', bicicletaController.bicicleta_delete_post);
module.exports = router;